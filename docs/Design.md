# Project Launcher Design

<span style="font-size: 1.6rem; font-weight: bold;">Table of Contents</span>

- [Project Launcher Design](#project-launcher-design)
  - [Overview](#overview)
  - [Configuration](#configuration)
    - [Configuration File Data Layout](#configuration-file-data-layout)
    - [Configuration Dialog](#configuration-dialog)
  - [Main Panel](#main-panel)
  - [Project Data Dialog](#project-data-dialog)
  - [Other Behaviors](#other-behaviors)

## Overview

This document contains the design decisions for the application.

## Configuration

The application provides a configuration file for each user. The location of the file is the .config/project-launcher directory in the user's home directory. 

### Configuration File Data Layout

The configuration data shall be stored in Javascript Object Notation (JSON) format, with the following layout:

```
{
    "config_file" : {
        "location" : "~/.config/project_launcher",
        "filename : "app_config.json"
    },
    "appearance" : {
        "background_color" : "darkgrey",
        "foreground_color" : "lightblue",
        "outline_color" : "lightblue",
        "font_size" : 12,
        "font_face" : "Helvetica"
    }
}
```
<sup>Figure 1: Configuration File Layout</sup>

### Configuration Dialog

The configuration data shall be managed via a custom dialog (see below):

<div style="text-align:center;">

![Configuration panel](images/Configuration-Panel.png)

<sup>Figure 2: Configuration Dialog
</div>

The configuration dialog provides a user-friendly way for the user to control some aspects of the appearance of the application and its controls.

## Main Panel

The main panel is presented to the user upon application start up. This panel is allows the user to add/remove/launch projects and configure the appearance of the application.

<div style="text-align: center;">

![Main Panel](images/Main-Panel.png)

<sup>Figure 3: Main Panel</sup>
</div>

__Control Descriptions__

- data grid - project names and descriptions
- '+' button - Launches the Project Data Dialog to add a new project
- '-' button - Remove project
- Pencil button - Launches the Project Data Dialog to edit project data
- 'Launch' button - Launch the project selected in the data grid
- Cog button - Configure the appearance of the application
- 'Close' button - Close the application

The user can also minimize, maximize, and close the application using the buttons in the title bar. Additionally, the user can launch a project by double-clicking on it in the data grid or edit the project configuration by right-clicking the project in the data grid.

## Project Data Dialog

The project data dialog is modal dialog that is displayed when the user adds a new project or edits an existing one.


<div style="text-align: center;">

![Project Data Dialog](images/Project-Data-Dialog.png)
<sup>Figure 4: Project Data Dialog</sup>

</div>

__Control Descriptions__

- Project Name - Text input for the name of the project
- Description - Text input for the description of the project
- Location - Text input for the location of the project
- Directory Picker - Data picker dialog that auto-fills the Location text input
- Commands - Text input for the commands to launch the editing environment
- Apply - Button to apply the changes for new and edited projects
- Cancel - Button to cancel any changes and close the dialog

## Other Behaviors

Cmd-M (Mac) / Ctrl-M (Windows/Linux) - Minimize the application
Double-click Minimized Icon - Restore application to the screen
Cmd-Q (Mac) / Ctrl-F4 (Windows) / Ctrl-X (Linux) - Close the application. If a modal dialog is active, the user will be prompted to save or discard the data.

The Tab key will allow the user to cycle between the various controls.

The Enter / Return key has the following behaviors:

| Control | Behavior |
| ---- | ---- |
| Text input | Moves to the next text input |
| Button | Activates the click event |

Should the user wish to have paragraph breaks in the project description (multi-line input), use of the Cmd-Enter (Mac) or Alt-Enter (Windows/Linux) should be used. As a general rule, multi-paragraph descriptions should be avoided.


