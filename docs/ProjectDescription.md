# Project Launcher

<span style="font-size: 1.6rem; font-weight: bold;">Table of Contents</span>


- [Project Launcher](#project-launcher)
  - [Overview](#overview)
  - [Features](#features)

## Overview

This application shall provide the capability to launch other applications. The initial use case is to provide a single launch point for software development and writing tools. The application itself shall run on any of the platforms providing a graphical user interface (GUI), Python 3 <a href="#fn-1" id="fnote1"><sup>[1]</sup></a>, and the PySimpleGui <a href="#fn-2" id="fnote2"><sup>[2]</sup></a> library.

## Features

The application shall provide the following features:

* Specify location of configuration file
* Specify the location of the data file
* Store project-related data
* Perform CRUD operations on the project data
* Perform CRUD operations on the configuration data
* Launch external applications and/or scripts
* Set the color scheme
* Set the font size for GUI elements

<hr width="35%" />

<sup id="fnote1">1</sup> https://python.org <a href="#fn-1">&larrhk;</a>
<sup id="fnote2">2</sup> https://pysimplegui.org <a href="#fn-2">&larrhk;</a>
