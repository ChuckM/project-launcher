# Project Launcher Requirements

<span style="font-size: 1.6rem; font-weight: bold;">Table of Contents</span>

- [Project Launcher Requirements](#project-launcher-requirements)
  - [Overview](#overview)
  - [Constraints](#constraints)
  - [Functional Requirements](#functional-requirements)
  - [Non-Functional Requirements](#non-functional-requirements)


## Overview

This document contains the requirements for the application.

## Constraints

<table style="width: 48rem;">
  <thead>
    <tr>
      <th width="12%">ID</th>
      <th width="12%">Status</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>C-01</td>
      <td></td>
      <td>The application shall support the Linux operating system.
    </tr>
    <tr>
      <td>C-02</td>
      <td></td>
      <td>The application shall support the Mac OS X operating system.</td>
    </tr>
    <tr>
      <td>C-03</td>
      <td></td>
      <td>The application shall support the Windows operating system.</td>
    </tr>
  </tbody>
</table>

## Functional Requirements

<table style="width: 48rem;">
  <thead>
    <tr>
      <th width="12%">ID</th>
      <th width="12%">Status</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>FR-01</td>
      <td></td>
      <td>Provide the Capability to store the application configuration in persistent storage.</td>
    </tr>
    <tr>
      <td>FR-02</td>
      <td>
      </td><td>Provide the capability to store project data in persistent storage.</td>
    </tr>
    <tr>
      <td>FR-03</td>
      <td>
      </td><td>Provide the capability to specify the location of the project data file in persistent storage.</td>
    </tr>
    <tr>
      <td>FR-04</td>
      <td></td>
      <td>Provide the capability to perform CRUD operations on the project data.</td>
    </tr>
    <tr>
      <td>FR-05</td>
      <td></td>
      <td>Provide the capability to perform CRUD operations on the configuration data.</td>
    </tr>
    <tr>
      <td>FR-06</td>
      <td></td>
      <td>Provide the capability to launch external applications and/or scripts.</td>
    </tr>
    <tr>
      <td>FR-07</td>
      <td></td>
      <td>Provide the capability to set the color scheme.</td>
    </tr>
    <tr>
      <td>FR-08</td>
      <td></td>
      <td>Provide the capability to set the font size for text elements.</td>
    </tr>
  </tbody>
</table>


## Non-Functional Requirements

<table style="width: 48rem;">
  <thead>
    <tr><th width="12%">ID</th><th width="12%">Status</th><th>Description</th></tr>
  </thead>
  <tbody>
    <tr>
      <td>NFR-01</td>
      <td></td>
      <td>The application shall provide a Graphical User Interface (GUI) for ease of use.</td>
    </tr>
  </tbody>
</table>
