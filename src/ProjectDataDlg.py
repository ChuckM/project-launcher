import os
import PySimpleGUI as sg


class ProjectDataDialog():

    """
    The ProjectDataDialog provides the entry / edit form used to
    add projects to the application.
    """

    def __init__(self, parms=None):
        super().__init__()

        if parms == None:
            self.parms = ['', '', '', '']
        else:
            self.parms = parms

    def make_window(self):
        layout = [
            [sg.Text('Project Name')],
            [sg.Input(default_text=self.parms[0],
                      expand_x=True, key='-PROJECT-NAME-')],
            [sg.Text('Description')],
            [sg.Input(default_text=self.parms[1],
                      expand_x=True, key='-DESCRIPTION-')],
            [sg.Text('Location')],
            [sg.Input(default_text=self.parms[2], expand_x=True, key='-LOCATION-'),
             sg.Button('',
                       auto_size_button=True,
                       button_type=sg.BUTTON_TYPE_BROWSE_FOLDER,
                       target=(sg.ThisRow, -1),
                       image_source='./icons/folder-light.png',
                       initial_folder=os.path.expanduser('~'))],
            [sg.Text('Commands')],
            [sg.Input(default_text=self.parms[3],
                      expand_x=True, key='-COMMANDS-')],
            [],
            [sg.Push(),
             sg.Button('Save', key='-SAVE-PROJECT-DATA-',
                       pad=((5, 5), (10, 5))),
             sg.Button('Cancel', key='-CANCEL-CHANGES-', pad=((5, 5), (10, 5)))]
        ]
        return sg.Window('Project Data',
                         layout,
                         modal=True,
                         font=('Consolas', 12))

    def run(self):

        values = []

        dialog = self.make_window()

       # starting the event loop
        while True:
            event, values = dialog.read()
            if (event == sg.WIN_CLOSED or event == '-CANCEL-CHANGES-'):
                values = self.parms
                break
            if (event == '-SAVE-PROJECT-DATA-'):
                if (len(values['-PROJECT-NAME-']) == 0):
                    sg.Popup('Missing project name')
                else:
                    break

        # closing the dialog window
        dialog.close()

        # sending the values dictionary back to the caller
        return values


if (__name__ == '__main__'):
    dlg = ProjectDataDialog()
    vals = dlg.run()
