import os
import platform
import pathlib
import PySimpleGUI as sg
import yaml


class ConfigMgr():
    """
    Configuration Manager
    """

    config = {}

    def __init__(self):
        system = platform.system()

        if system == 'Windows':
            self.cfg_dir = pathlib.Path(os.environ.get('APPDATA'), 'pm')
        else:
            self.cfg_dir = pathlib.Path.home() / '.config' / 'pm'

        # test for existing config file
        self.cfg_file = pathlib.Path(self.cfg_dir, 'config.yaml')
        if os.path.isfile(self.cfg_file) == False:
            self.config['datafile'] = sg.popup_get_folder(
                'Select the location for your datafile.',
                title='Data File Location',
                default_path=pathlib.Path.home(),
                initial_folder=pathlib.Path.home(),
                modal=True)
            os.makedirs(self.cfg_dir)
            with open(f'{self.cfg_dir}/config.yaml', 'w') as f:
                yaml.dump(self.config, f)
        else:
            with open(f'{self.cfg_dir}/config.yaml', 'r') as f:
                self.config = yaml.full_load(f)


if __name__ == '__main__':
    cfg_mgr = ConfigMgr()
