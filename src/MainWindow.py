import os
import json
from pathlib import Path, PurePath
import PySimpleGUI as sg
import ProjectDataDlg as pdd


class MainWindow():

    # projects = []
    data_values = []
    headings = ['  Name ', ' Description ', ' Location ', ' Command ']
    table_row_selected = None
    app_dir = os.getcwd()
    print(f'app_dir: {app_dir}')
    datadir = f'{app_dir}/data'
    datafile = 'projects.json'

    layout = [[sg.Table(values=data_values,
                        headings=headings,
                        visible_column_map=[True, True, False, False],
                        auto_size_columns=True,
                        num_rows=20,
                        def_col_width=20,
                        justification="left",
                        font=('Consolas', 14),
                        header_font=('Consolas', 12),
                        pad=4,
                        enable_events=True,
                        # enable_click_events=True,
                        select_mode=sg.TABLE_SELECT_MODE_BROWSE,
                        key='-PROJECTS-TABLE-'),
               ],
              [sg.Button(' +',
                         font=('Consolas', 12),
                         size=(2, 1),
                         enable_events=True,
                         key='-ADD-ENTRY-'),

               sg.Button(' –',
                         size=(2, 1),
                         font=('Consolas', 12),
                         enable_events=True,
                         key='-REMOVE-ENTRY-'),
               sg.Push(),
               sg.Button(' Launch',
                         font=('Consolas', 12),
                         enable_events=True,
                         key='-LAUNCH-'),
               sg.Button(' Close',
                         font=('Consolas', 12),
                         enable_events=True,
                         key='-CLOSE-')]]

    def __init__(self):
        super().__init__()
        self.load_projects()

    def reload_window(self):
        pass

    def save_projects(self):
        '''
        save_projects converts the contents of data_values
        to an array of objects.
        '''

        if len(self.data_values) > 0:
            projects = []
            # convert data_values arrays to objects
            for entry in self.data_values:
                projects.append({'Name': entry[0],
                                 'Description': entry[1],
                                 'Location': entry[2],
                                 'Command': entry[3]})

            with open(f'{self.datadir}/{self.datafile}', 'w', encoding='utf8') as f:
                json.dump(projects, f)

    def load_projects(self):
        """
        The load projects function loads the list of project objects
        """
        project_list = []

        # check to see if the path exists
        if (os.path.exists(f'{self.datadir}') == False):
            print('no {self.datadir}')
            os.mkdir(self.datadir)

        # check to see if the data file exists
        if (os.path.exists(f'{self.datadir}/{self.datafile}') == False):
            with open(f'{self.datadir}/{self.datafile}', 'w', encoding='utf8') as f:
                json.dump(project_list, f)
        else:
            project_list = json.load(open(f'{self.datadir}/{self.datafile}'))

        if (len(project_list) > 0):
            for proj in project_list:
                self.data_values.append(
                    [
                        proj['Name'],
                        proj['Description'],
                        proj['Location'],
                        proj['Command']
                    ]
                )

    def run(self):
        window = sg.Window('Project Launcher', self.layout,
                           finalize=True,
                           use_custom_titlebar=True,
                           titlebar_font=('Consolas', 14))
        window['-PROJECTS-TABLE-'].bind('<Double-Button-1>',
                                        '+-double click-')

        while True:
            event, values = window.read()
            # close the window
            if (event == sg.WIN_CLOSED or event == '-CLOSE-'):
                break

            # print(f'event: |{event}| values: {values}')

            if event == '-PROJECTS-TABLE-+-double click-':
                dlg = pdd.ProjectDataDialog(self.table_row_selected)
                edited_entry = dlg.run()
                self.table_row_selected[0] = edited_entry[0]
                self.table_row_selected[1] = edited_entry[1]
                self.table_row_selected[2] = edited_entry[2]
                self.table_row_selected[3] = edited_entry[3]
                window['-PROJECTS-TABLE-'].update(values=self.data_values)

                self.save_projects()
                window.refresh()

            if event == '-PROJECTS-TABLE-':
                try:
                    self.table_row_selected = [self.data_values[row]
                                               for row in values[event]][0]
                except IndexError:
                    continue

            if event == '-ADD-ENTRY-':
                dlg = pdd.ProjectDataDialog()
                new_entry = dlg.run()
                if new_entry != None:
                    self.data_values.append(
                        new_entry
                    )
                    window['-PROJECTS-TABLE-'].update(self.data_values,
                                                      num_rows=20,
                                                      visible=True)
                    self.save_projects()
                window.refresh()

            if event == '-REMOVE-ENTRY-':
                self.data_values.remove(self.table_row_selected)
                window['-PROJECTS-TABLE-'].update(self.data_values,
                                                  num_rows=20,
                                                  visible=True)
                self.save_projects()
                window.refresh()

            if event == '-LAUNCH-':
                directory = os.path.expanduser(self.table_row_selected[2])
                command = self.table_row_selected[3]
                try:
                    if directory != None and directory != '':
                        os.chdir(directory)
                        if command != None and command != '':
                            os.system(command)
                except FileNotFoundError:
                    sg.popup_error(f'Missing directory: {directory}')

                os.chdir(self.app_dir)
                # window.refresh()

        window.close()


if (__name__ == '__main__'):
    window = MainWindow()
    window.run()
